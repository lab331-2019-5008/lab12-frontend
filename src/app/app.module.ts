import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsViewComponent } from './students/view/students.view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatRadioModule, MatProgressBarModule
} from '@angular/material';
import { MatFileUploadModule } from 'mat-file-upload';
import { MatInputModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { StudentTableComponent } from './students/student-table/student-table.component';
import { StudentRestImplService } from  './service/student-rest-impl.service';
import { CourseListComponent } from './course/course-list/course-list.component';
import { CourseService } from './service/course-service';
import { LecturerService } from './service/lecturer-service';
import { CourseRestImplService } from './service/course-rest-impl.service';
import { LecturerRestImplService } from './service/lecturer-rest-impl-service';
import { CourseFileImplService } from './service/course-file-impl.service';
import { CourseAddComponent } from './course/course-add/course-add.component';
import { MatSelectModule } from '@angular/material/select';
import { CourseInfoComponent } from './course/course-info/course-info.component';
import { LoginComponent } from './shared/login-component/login-component.component';
import { JwtInterceptorService } from './helper/jwt-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    CourseListComponent,
    CourseAddComponent,
    CourseInfoComponent,
    LoginComponent,
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatProgressBarModule,
    StudentRoutingModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatFileUploadModule,
    MatSelectModule
  ],
  providers: [
    { // provide: StudentService, useClass: StudentsFileImplService
      provide: StudentService, useClass: StudentRestImplService },
      //select between mock-up and server
      { provide: CourseService, useClass: CourseRestImplService },
      // { provide: CourseService, useClass: CourseFileImplService },
      { provide: LecturerService, useClass: LecturerRestImplService },
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
