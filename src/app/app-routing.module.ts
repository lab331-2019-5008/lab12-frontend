import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { CourseListComponent } from './course/course-list/course-list.component';
import { CourseAddComponent } from './course/course-add/course-add.component';
import { CourseInfoComponent } from './course/course-info/course-info.component';
import { LoginComponent } from './shared/login-component/login-component.component';
import { LecturerGuard } from './guard/lecturer.guard';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent },
  { path: 'course/list', component: CourseListComponent, canActivate: [LecturerGuard] },
  { path: 'course/add', component: CourseAddComponent, canActivate: [LecturerGuard] },
  { path: 'course/:id', component: CourseInfoComponent,canActivate: [LecturerGuard] },
  { path: '**', component: FileNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
